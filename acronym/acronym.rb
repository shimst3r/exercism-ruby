# frozen_string_literal: true

# Acryonym implements functionality for creating acronyms from words.
class Acronym
  def self.abbreviate(words)
    words
      .scan(/\b[a-zA-Z]/)
      .join
      .upcase
  end
end

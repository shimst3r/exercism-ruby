# frozen_string_literal: true

require 'set'

class Pangram
  def self.pangram?(sentence)
    Set.new('a'..'z') <= Set.new(sentence.downcase.chars)
  end
end

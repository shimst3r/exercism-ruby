# frozen_string_literal: true

# Given a list of color names, return their resistor color duo number.
class ResistorColorDuo
  COLORS = %w[black brown red orange yellow green blue violet grey white].freeze

  def self.value(lst)
    lst.take(2).map { |string| COLORS.index(string) }.join.to_i
  end
end
